package test.util

import org.scalatest._
import matchers.MustMatchers._

class ConfigSpec extends WordSpec with test.Helpers {

  "Config wrapper" when {
    "loaded" should {
      "default to standard list of configurations" in {
        util.Config() must not be ('empty)
      }

      "allow custom paths to be specified" in {
        util.Config("couchdb.properties") must not be ('empty)
      }

      "return Options" in {
        util.Config.default.getConfig("couchdb") match {
          case Some(config) =>

            config must be(anInstanceOf[util.Config])

            // String:
            config.getString("username") must equal(Some("some_username"))
            config.getString("bob") must equal(None)

            // Int:
            config.getInt("port") must be(anInstanceOf[Option[Int]])

            // Boolean:
            config.getBoolean("https") must be(anInstanceOf[Option[Boolean]])

          case _ => fail( """Configuration key "couchdb" not found!""")
        }
      }
    }
  }
}
