package test

trait Helpers {

  import org.scalatest.matchers._

  def anInstanceOf[T: Manifest] = BeMatcher {
    obj: Any =>
      val cls = manifest[T].runtimeClass.asInstanceOf[Class[T]]
      MatchResult(
        cls.isAssignableFrom(obj.getClass),
        s"Expected $obj to be an instance of $cls but was an instance of ${obj.getClass}",
        obj.toString + " was an instance of " + cls.toString
      )
  }
}