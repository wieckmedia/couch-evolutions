package test

import org.bitbucket.wieckmedia.evolutions._

class EvolutionsSpec extends Spec {

  "Evolutions" - {
    "with an empty database" - {
      "should describe evolutions to be run" in {
        pendingUntilFixed {
          "Evolutions.diff"
        }
      }

      "should create each design document" in {
        pendingUntilFixed {
          'foo
        }
      }
      "should summarize the design documents created" in {
        pending
      }
    }

    "with existing design documents" - {
      "should create new versions of each design document" in {
        pending
      }

      "should summarize the design documents revised" in {
        pending
      }

      "should not automatically destroy the pre-existing design documents" in {
        pending
      }
    }

    "with a malformed design document" - {
      "should report an error for the specific design document" in {
        pending
      }
    }

    "with " - {
      "should be pending" in {
        pending
      }
    }
  }

}
