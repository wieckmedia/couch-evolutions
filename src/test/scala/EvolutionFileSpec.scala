import org.bitbucket.wieckmedia.evolutions.EvolutionFile._
import org.scalatest._
import matchers.MustMatchers._
import org.bitbucket.wieckmedia.evolutions.EvolutionFile
import sprouch.MapReduce

class EvolutionFileSpec extends WordSpec with test.Helpers {

  val Map = "(doc) -> emit(doc._id, doc)"
  val Reduce = "(doc, a) -> a + 1"

  "An EvolutionFile" when {

    "given a script with just a map" should {
      val source = s"""
                     |# --- !Map
                     |$Map
                   """.stripMargin.split('\n')

      "be valid" in {
        EvolutionFile parse source must be('success)
      }

      "provide a MapReduce" which {
        val mr = EvolutionFile parse source getOrElse MapReduce("", None)

        "has a map" in {
          mr.map must equal(Map)
        }

        "does not have a map" in {
          mr.reduce must be('empty)
        }

      }
    }

    "given a script with a map and reduce" should {

      val source = s"""
                     |# --- !Map
                     |$Map
                     |
                     |# --- !Reduce
                     |$Reduce
                   """.stripMargin.split('\n')

      "be valid" in {
        EvolutionFile parse source must be('success)
      }

      "provide a MapReduce" which {
        val mr = EvolutionFile parse source getOrElse MapReduce("", None)

        "has a map" in {
          mr.map must equal(Map)
        }

        "has a reduce" in {
          mr.reduce must equal(Some(Reduce))
        }
      }

    }

    "given a script with a reduce then a map" should {

      val source = s"""
                     |# --- !Reduce
                     |$Reduce
                     |
                     |# --- !Map
                     |$Map
                   """.stripMargin.split('\n')

      "be valid" in {
        EvolutionFile parse source must be('success)
      }

      "provide a MapReduce" which {
        val mr = EvolutionFile parse source getOrElse MapReduce("", None)

        "has a map" in {
          mr.map must equal(Map)
        }

        "has a reduce" in {
          mr.reduce must equal(Some(Reduce))
        }
      }

    }

    "given a script with no map" should {
      val source = s"""
                     |# --- !Reduce
                     |abc123
                   """.stripMargin.split('\n')

      "not be valid" in {
        intercept[MissingMapException] {
          EvolutionFile parse source get
        }
      }
    }

    "given a script with multiple maps" should {
      val source = s"""
                     |# --- !Map
                     |abc123
                     |# --- !Map
                     |def456
                   """.stripMargin.split('\n')

      "not be valid" in {
        intercept[MultipleMapException] {
          EvolutionFile parse source get
        }
      }
    }

    "given a script with multiple reduces" should {
      val source = s"""
                     |# --- !Map
                     |1235
                     |# --- !Reduce
                     |abc123
                     |# --- !Reduce
                     |def456
                   """.stripMargin.split('\n')

      "not be valid" in {
        intercept[MultipleReduceException] {
          EvolutionFile parse source get
        }
      }
    }
  }
}