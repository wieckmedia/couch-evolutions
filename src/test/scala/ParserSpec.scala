package test

import org.scalatest._
import matchers.MustMatchers._
import org.bitbucket.wieckmedia.evolutions.{Parser => EvolutionsParser}
import EvolutionsParser._

class ParserSpec extends WordSpec with test.Helpers {

  object Parser {
    def apply(string: String): MapsAndReduces = EvolutionsParser(string.split('\n'))
  }

  "Parser" should {

    "extract a single map" in {
      val result = Parser( """
                             |# --- !Map
                             |abc123
                           """.stripMargin)
      result.maps must contain("abc123")
    }

    "extract multiple maps" in {
      val result = Parser( """
                             |# --- !Map
                             |abc123
                             |# --- !Map
                             |def456
                           """.stripMargin)
      result.maps must contain("abc123")
      result.maps must contain("def456")
    }

    "extract a single reduce" in {
      val result = Parser( """
                             |# --- !Reduce
                             |abc123
                           """.stripMargin)
      result.reduces must contain("abc123")
    }

    "extract multiple reduces" in {
      val result = Parser( """
                             |# This is a comment
                           """.stripMargin)
      result.maps must be('empty)
    }

    "extract a map and a reduce in presence of a comment" in {
      val result = Parser( """
                             |# This is a comment
                             |
                             |# --- !Map
                             |this is my map
                             |
                             |# --- !Reduce
                             |this is my reduce
                           """.stripMargin)
      result.maps must contain("this is my map")
      result.reduces must contain("this is my reduce")
    }

  }

}
