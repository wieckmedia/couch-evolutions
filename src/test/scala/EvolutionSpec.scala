package test

import org.bitbucket.wieckmedia.evolutions._
import sprouch.{Views, MapReduce}

class EvolutionSpec extends Spec {

  "An Evolution" - {
    val mr = MapReduce("(doc) -> emit(doc._id, 1)", Some("(doc, a) -> a + 1"))
    val views = Views(Map("sum" -> mr), language = "javascript")

    "require a list of Views" in {
      Evolution("docs", views)
    }

    "have a sha1" in {
      Evolution("docs", views).sha1
    }

    "calculate the sha1 of all views" in {
      println(Evolution("docs", views))
//      Evolution("docs", views).sha1 must equal("abcdefg")
    }
  }
}