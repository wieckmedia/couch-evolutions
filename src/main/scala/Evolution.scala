package org.bitbucket.wieckmedia.evolutions

import sprouch.{MapReduce, Views}
import com.roundeights.hasher.Hasher

case class Evolution(designDocumentName:String, views:Views) {
  def sha1 = Hasher("bob").sha1

  def foo = {
    def sanitize(f:String) = f.trim
    Hasher(views.views.foldLeft(List.empty[String]) {
      case (a, (name, MapReduce(map, Some(reduce)))) => (name + sanitize(map) + sanitize(reduce)) :: a
      case (a, (name, MapReduce(map, None))) => (name + sanitize(map)) :: a
    }.mkString).sha1
  }
}