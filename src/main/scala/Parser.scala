package org.bitbucket.wieckmedia.evolutions

import org.bitbucket.wieckmedia.evolutions.util._

object Parser {
  case class MapsAndReduces(maps: Seq[String], reduces: Seq[String])

  def apply(lines: Seq[String]) = new Parser(lines).parse
}

class Parser(lines:Seq[String]) {
  import Parser.MapsAndReduces

  def parse: MapsAndReduces = {
    val MapMarker = """^#.*!Map.*$""".r
    val ReduceMarker = """^#.*!Reduce.*$""".r

    val MapsTag = "MAPS"
    val ReducesTag = "REDUCES"
    val UnknownTag = "UNKNOWN"

    val mapMapsAndReduces: PartialFunction[String, String] = {
      case MapMarker() => MapsTag
      case ReduceMarker() => ReducesTag
      case _ => UnknownTag
    }

    val isMarker: PartialFunction[String, Boolean] = {
      case MapMarker() => true
      case ReduceMarker() => true
      case _ => false
    }

    val parsed = Collections.unfoldLeft(("", lines.toList.map(_.trim))) {
      case (_, Nil) => None
      case (context, lines) => {
        val (some, next) = lines.span(l => !isMarker(l))
        Some(
          (
            next.headOption.map(c => (mapMapsAndReduces(c), next.tail)).getOrElse("" -> Nil),
            context -> some.mkString("\n")
            )
        )
      }
    }.reverse.drop(1).groupBy(i => i._1).mapValues {
      _.map(_._2.trim)
    }

    MapsAndReduces(parsed.get(MapsTag).getOrElse(Seq()), parsed.get(ReducesTag).getOrElse(Seq()))
  }

}
