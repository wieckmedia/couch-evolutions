package org.bitbucket.wieckmedia.evolutions

import java.io.{ File => JavaFile }
import scala.util.{Try, Success, Failure}
import sprouch.MapReduce
import org.bitbucket.wieckmedia.evolutions.Parser.MapsAndReduces

object EvolutionFile {

  class MalformedEvolutionException(msg: String, functions:MapsAndReduces) extends RuntimeException(msg)

  class MissingMapException(functions:MapsAndReduces) extends MalformedEvolutionException("No Map definition present", functions)

  class MultipleMapException(functions:MapsAndReduces) extends MalformedEvolutionException(
    "Multiple Map definitions found, only a single Map definition is allowed", functions)

  class MultipleReduceException(functions:MapsAndReduces) extends MalformedEvolutionException(
    "Multiple Reduce definitions found, only a single (optional) Reduce definition is allowed", functions)

  def parse(lines:Seq[String]):Try[MapReduce] = {
    //      scala.io.Source.fromFile("file.txt")

    val result = Parser(lines)
    result match {
      case MapsAndReduces(map :: Nil, Nil) => Success(MapReduce(map, None))
      case MapsAndReduces(map :: Nil, reduce :: Nil) => Success(MapReduce(map, Some(reduce)))
      case MapsAndReduces(Nil, _) => Failure(new MissingMapException(result))
      case MapsAndReduces(map :: tail :: Nil, _) => Failure(new MultipleMapException(result))
      case MapsAndReduces(_, reduce :: tail :: Nil) => Failure(new MultipleReduceException(result))
      case _ => Failure(new MalformedEvolutionException("Unknown parse error", result))
    }
  }

}