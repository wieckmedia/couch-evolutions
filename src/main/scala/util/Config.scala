package util

import com.typesafe.config.{ConfigFactory, Config => TypeSafeConfig}

class Config(config: TypeSafeConfig) {

  def isEmpty = config.isEmpty

  def getConfig(path: String): Option[Config] = {
    guard(path)(new Config(config.getConfig(path)))
  }

  def getString(path: String): Option[String] = {
    guard(path)(config.getString(path))
  }

  def getInt(path: String): Option[Int] = {
    guard(path)(config.getInt(path))
  }

  def getBoolean(path: String): Option[Boolean] = {
    guard(path)(config.getBoolean(path))
  }

  private def guard[T](path: String)(success: => T): Option[T] = {
    if (config.hasPath(path))
      Some(success)
    else
      None
  }
}

object Config {

  def apply(configurationFilePathInClassPath: String = null): Config = {
    Option(configurationFilePathInClassPath).fold {
      ConfigFactory.load()
    } {
      path =>
        ConfigFactory.load(path)
    } match {
      case config: TypeSafeConfig => new Config(config)
    }
  }

  def default = apply()
}
