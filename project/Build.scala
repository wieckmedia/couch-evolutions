import sbt._
import Keys._

object ApplicationBuild extends Build {

  lazy val couchEvolutions = Project("couch-evolutions", file("."))
    .settings(
      scalaVersion := "2.10.1",
      version := "1.0-SNAPSHOT",
      resolvers := Seq(
        "Maven Central" at "http://repo1.maven.org/maven2/",
        "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
      ),
      libraryDependencies ++= Seq(
        "org.scalatest" %% "scalatest" % "2.0.M5b" % "test",
        "com.typesafe" % "config" % "1.0.0"
      ),
    testOptions in Test := Nil,
    parallelExecution in Test := false,
    scalacOptions ++= Seq("-language:postfixOps", "-feature", "-unchecked", "-deprecation"),
    evolveTask
  ) dependsOn(sprouch, hasher)

  lazy val sprouch = RootProject(uri("git://github.com/sam/sprouch.git#2.10"))

  lazy val hasher = RootProject(uri("git://github.com/Nycto/Hasher.git"))

  lazy val evolveTask = TaskKey[Unit]("evolve", "Ensures CouchDB Views are current") := {
    println("Hello Funky World!")
  }
}
